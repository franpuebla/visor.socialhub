FROM nginx

LABEL mantainer="pueblafran@gmail.com"

COPY src/ /usr/share/nginx/html

EXPOSE 80
